package tablutserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
 
public class TablutServer {
    private static Socket mSocketPlayer1, mSocketPlayer2;
    private static BufferedReader mBufferedReaderPlayer1;
    private static BufferedReader mBufferedReaderPlayer2;
    private static BufferedWriter mBufferedWriterPlayer1;
    private static BufferedWriter mBufferedWriterPlayer2;
 
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(1234);    
            System.out.println("Servidor Ativo !!");
 
            mSocketPlayer1 = serverSocket.accept();
            System.out.println("Player 1 Conectou-se !!");
            
            mSocketPlayer2 = serverSocket.accept();
            System.out.println("Player 2 Conectou-se !!");
            
            mBufferedReaderPlayer1 = new BufferedReader(new InputStreamReader(mSocketPlayer1.getInputStream()));
            mBufferedReaderPlayer2 = new BufferedReader(new InputStreamReader(mSocketPlayer2.getInputStream()));
             
            mBufferedWriterPlayer1 = new BufferedWriter(new OutputStreamWriter(mSocketPlayer1.getOutputStream()));
            mBufferedWriterPlayer2 = new BufferedWriter(new OutputStreamWriter(mSocketPlayer2.getOutputStream()));
                                                     
             final TablutTableValue tablutTable = new TablutTableValue();                   
             sendTableUpdated(tablutTable.initializeTableBoardValues()); 
             
            sendPlayersId();
                       
            sendWhoPlaysNow(1);
            
            int round = 1;
            int playerWinner = 0;
            while(playerWinner == 0) {
                Integer toLine, toColumn, fromLine, fromColumn;
                if (round % 2 != 0) {
                    toLine = mBufferedReaderPlayer1.read();
                    toColumn = mBufferedReaderPlayer1.read();
                    fromLine = mBufferedReaderPlayer1.read();
                    fromColumn = mBufferedReaderPlayer1.read();
                    
                    tablutTable.updateTableValues(toLine, toColumn, fromLine, fromColumn, 1);
                    
                    playerWinner = tablutTable.verifyGameOver(1);
                } else {        
                    toLine = mBufferedReaderPlayer2.read();
                    toColumn = mBufferedReaderPlayer2.read();
                    fromLine = mBufferedReaderPlayer2.read();
                    fromColumn = mBufferedReaderPlayer2.read();
                    
                    tablutTable.updateTableValues(toLine, toColumn, fromLine, fromColumn, 2);
                    
                    playerWinner = tablutTable.verifyGameOver(2);
                }
                
                sendCapturedPieces(tablutTable.getCapturedBlackPices(), tablutTable.getCapturedWhitePices());
                               
                sendTableUpdated(tablutTable.getTablutBoardValue()); 
                
                sendWinnerPlayer(playerWinner);               
                
                round++;
                
                if (round % 2 != 0) {
                    sendWhoPlaysNow(1);
                } else {
                    sendWhoPlaysNow(2);                
                }                                                            
            }                 
        } catch (Exception e) {}
                
        finally {
            try {
                mBufferedReaderPlayer1.close();
                mBufferedReaderPlayer2.close();
                mBufferedWriterPlayer1.close();
                mBufferedWriterPlayer2.close();
                
                mSocketPlayer1.close();
                mSocketPlayer2.close();
            } catch(Exception e){}
        }
    }
    
    private static void sendPlayersId() throws IOException {
        mBufferedWriterPlayer1.write(1);
        mBufferedWriterPlayer1.flush(); 

        mBufferedWriterPlayer2.write(2);
        mBufferedWriterPlayer2.flush();
    }
    
    private static void sendTableUpdated(Integer matrix[][]) throws IOException {
        for (Integer line = 0; line < 9; line++) {
             for (Integer column = 0; column < 9; column++) {
                mBufferedWriterPlayer1.write(matrix[line][column]);               
                mBufferedWriterPlayer2.write(matrix[line][column]); 
            }
        }
        mBufferedWriterPlayer1.flush();
        mBufferedWriterPlayer2.flush();
    }
    
    private static void sendWhoPlaysNow(int turn) throws IOException {
        mBufferedWriterPlayer1.write(turn); 
        mBufferedWriterPlayer1.flush();
        
        mBufferedWriterPlayer2.write(turn);
        mBufferedWriterPlayer2.flush();
    }
    
    private static void sendCapturedPieces(int capturedBlackPieces, int capturedWhitePieces) throws IOException {
        mBufferedWriterPlayer1.write(capturedBlackPieces);
        mBufferedWriterPlayer1.write(capturedWhitePieces);               
        mBufferedWriterPlayer1.flush();
        
        mBufferedWriterPlayer2.write(capturedBlackPieces);
        mBufferedWriterPlayer2.write(capturedWhitePieces);
        mBufferedWriterPlayer2.flush();
    }
    
    private static void sendWinnerPlayer(int winnerPlayer) throws IOException {
        mBufferedWriterPlayer1.write(winnerPlayer);
        mBufferedWriterPlayer1.flush();
        
        mBufferedWriterPlayer2.write(winnerPlayer); 
        mBufferedWriterPlayer2.flush();
    }   
}