package tablutserver;

public class TablutTableValue {
    
    private Integer[][] mTablutBoardValue;
    private int mCapturedWhitePices = 0, mCapturedBlackPices = 0;
    
    public Integer[][] initializeTableBoardValues() {
        mTablutBoardValue = (new Integer[][]{
            {0, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 2, 0, 0, 0, 0},
            {1, 0, 0, 0, 2, 0, 0, 0, 1},
            {1, 1, 2, 2, 3, 2, 2, 1, 1},
            {1, 0, 0, 0, 2, 0, 0, 0, 1},
            {0, 0, 0, 0, 2, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 0, 0, 0}
        });
        
        return mTablutBoardValue;
    }
    
     public void updateTableValues(Integer toLine, Integer toColumn, Integer fromLine, Integer fromColumn, int player) {
        mTablutBoardValue[toLine][toColumn] = mTablutBoardValue[fromLine][fromColumn];
        mTablutBoardValue[fromLine][fromColumn] = 0;
        
        if (mTablutBoardValue[toLine][toColumn] != 3) {
            int enemie = player == 1 ? 2 : 1;
            if (toLine + 1 < 9 && mTablutBoardValue[toLine + 1][toColumn] == enemie && mTablutBoardValue[toLine + 1][toColumn] != 3 &&
                    toLine + 2 < 9 && mTablutBoardValue[toLine + 2][toColumn] == player && mTablutBoardValue[toLine + 2][toColumn] != 3) {
                mTablutBoardValue[toLine + 1][toColumn] = 0;
                if (player == 1) mCapturedWhitePices ++;
                else mCapturedBlackPices++;
            }

            if (toLine - 1 >= 0 && mTablutBoardValue[toLine - 1][toColumn] == enemie && mTablutBoardValue[toLine - 1][toColumn] != 3 &&
                    toLine - 2 >= 0 && mTablutBoardValue[toLine - 2][toColumn] == player && mTablutBoardValue[toLine - 2][toColumn] != 3) {
                mTablutBoardValue[toLine - 1][toColumn] = 0;
                if (player == 1) mCapturedWhitePices ++;
                else mCapturedBlackPices++;
            }

            if (toColumn + 1 < 9 && mTablutBoardValue[toLine][toColumn + 1] == enemie && mTablutBoardValue[toLine][toColumn + 1] != 3 &&
                    toColumn + 2 < 9 && mTablutBoardValue[toLine][toColumn + 2] == player && mTablutBoardValue[toLine][toColumn + 2] != 3) {
                mTablutBoardValue[toLine][toColumn + 1] = 0;
                if (player == 1) mCapturedWhitePices ++;
                else mCapturedBlackPices++;
            }

            if (toColumn - 1 >= 0 && mTablutBoardValue[toLine][toColumn - 1] == enemie && mTablutBoardValue[toLine][toColumn - 1] != 3 &&
                    toColumn - 2 >= 0 && mTablutBoardValue[toLine][toColumn - 2] == player && mTablutBoardValue[toLine][toColumn - 2] != 3) {
                mTablutBoardValue[toLine][toColumn - 1] = 0;
                if (player == 1) mCapturedWhitePices ++;
                else mCapturedBlackPices++;
            }
        }
    }
     
    public int verifyGameOver(int player) {
        int playerWinner = 0;
        int enemie = player == 1 ? 2 : 1;
        boolean enemieCanPlay = false;
        for (Integer line = 0; line < 9; line++) {
            for (Integer column = 0; column < 9; column++) {                    
                 if ((line == 0 || column == 0) && mTablutBoardValue[line][column] == 3) {
                   playerWinner = 2;
                   break;
                } else if (mTablutBoardValue[line][column] == 3 && 
                        line + 1 < 9 && (mTablutBoardValue[line + 1][column] == 1 || line + 1 == 4 && column == 4) 
                        && line - 1 >= 0 && (mTablutBoardValue[line - 1][column] == 1 || line - 1 == 4 && column == 4)
                        && column + 1 < 9 && (mTablutBoardValue[line][column + 1] == 1 || line == 4 && column + 1 == 4)
                        && column - 1 >= 0 && (mTablutBoardValue[line][column - 1] == 1 || line == 4 && column - 1 == 4)) {

                   playerWinner = 1;
                   break; 
                } else if (!enemieCanPlay && mTablutBoardValue[line][column] == enemie || (enemie == 2 && 
                       mTablutBoardValue[line][column] == 3)) {
                   enemieCanPlay = true;                    
                }
            }          
        }
        if (!enemieCanPlay) {
          playerWinner = player;
        }
        
        return playerWinner;
    }
     
    public Integer[][] getTablutBoardValue() {
        return mTablutBoardValue;
    }
    
    public int getCapturedWhitePices() {
        return mCapturedWhitePices;
    }
    
    public int getCapturedBlackPices() {
        return mCapturedBlackPices;
    }
}
